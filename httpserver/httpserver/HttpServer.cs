﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace httpserver
{
    
    class HttpServer
    {

        bool running = false;
        TcpListener listener;

        public HttpServer(int port)
            {
                listener = new TcpListener(IPAddress.Any,port);
            }

        public void Start()
        {
            Thread thread = new Thread(new ThreadStart(Run));
            thread.Start();
        }


        private void Run()
        {
            listener.Start();
            running = true;

            Console.WriteLine("Start Listener...");

            while (running)
            {
                TcpClient client = listener.AcceptTcpClient();
                HandleClient(client);
                
                client.Close();
               
            }

        }
        private void HandleClient(TcpClient client)
        {
            StreamReader reader = new StreamReader(client.GetStream());
            string message = "";

            while (reader.Peek() != -1 )
            {
                message += reader.ReadLine();
            }
            Console.WriteLine("request: {0}", message);

            
            string response = Request.GetRequest(message);

            System.IO.StreamWriter writer = new System.IO.StreamWriter(client.GetStream());
            writer.WriteLine(response);

        }


    }
}
