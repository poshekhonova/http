﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Text;
using System.Data.Common;
using System.Linq;
using Newtonsoft.Json;
using System.Net.Sockets;

namespace httpserver
{
    class Request
    {

        public static string GetRequest(string message)
        {
            string response ="";
            const string databaseName = "patient.db";
            if (String.IsNullOrEmpty(message))
            {
                return null;
            }

            if (message.Split()[0] == "GET")
            {
                Patient data = new Patient();
                string parametrs = message.Split()[1];
                string parametr = parametrs.Split("/?")[0];
                string value = parametrs.Split('=')[1];

                    SQLiteConnection connection =
                        new SQLiteConnection(string.Format("Data Source={0};", databaseName));
                    connection.Open();
                    SQLiteCommand command = new SQLiteCommand("SELECT lastname, firstname, middlename, snils, enp FROM patient WHERE" + parametr +" = " + value, connection);
                    SQLiteDataReader reader;
                    reader = command.ExecuteReader();


                    while (reader.Read())
                    {
                        data.Lastname = reader["lastname"].ToString();
                        data.Firstname = reader["firstname"].ToString();
                        data.Middlename = reader["middlename"].ToString();
                        data.Snils = Convert.ToInt32(reader["snils"]);
                        data.Enp = Convert.ToInt32(reader["enp"]);


                        Console.WriteLine(data.Lastname);
                        Console.WriteLine(data.Firstname);
                        Console.WriteLine(data.Middlename);
                        Console.WriteLine(data.Snils.ToString());
                        Console.WriteLine(data.Enp.ToString());
                    }

                    response = "{lasname:" + data.Lastname + ",/nfirstname:" + data.Firstname + ",/ nmiddlename:" + data.Middlename + ",/nsnils:" + data.Snils.ToString() + ",/nenp:" + data.Enp.ToString() + "}";
                
                    connection.Close();
                

            }
            if (message.Split()[0] == "POST")
            {
                string msg = '{' + message.Split('{').Last();

                Patient patient = JsonConvert.DeserializeObject<Patient>(msg);
                
                SQLiteConnection connection =
                    new SQLiteConnection(string.Format("Data Source={0};", databaseName));
                connection.Open();
                patient.Lastname = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(patient.Lastname.ToLower());
                patient.Middlename = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(patient.Middlename.ToLower());
                patient.Firstname = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(patient.Firstname.ToLower());
                SQLiteCommand command = new SQLiteCommand("INSERT INTO patient ('lastname', 'firstname','middlename','snils','enp') VALUES ('" + patient.Lastname + "','" + patient.Firstname + "','" + patient.Middlename + "'," + patient.Snils + "," + patient.Enp + ");", connection);
                command.ExecuteNonQuery();
                response = "{lasname:" + patient.Lastname + ",/nfirstname:" + patient.Firstname + ",/ nmiddlename:" + patient.Middlename + ",/nsnils:" + patient.Snils.ToString() + ",/nenp:" + patient.Enp.ToString() + "}";
                connection.Close();
            }
            if (message.Split()[0] == "PUT")
            {
                Patient data = new Patient();
                string parametrs = message.Split()[1];
                if (parametrs.Split('=')[0] == "/?id")
                {
                    string id = parametrs.Split('=')[1];

                    string msg = '{' + message.Split('{').Last();

                    Patient patient = JsonConvert.DeserializeObject<Patient>(msg);

                    SQLiteConnection connection =
                        new SQLiteConnection(string.Format("Data Source={0};", databaseName));
                    connection.Open();
                    SQLiteCommand command = new SQLiteCommand("UPDATE patient SET lastname=" + System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(patient.Lastname.ToLower()) + ",firstname=" + System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(patient.Firstname.ToLower()) + ", middlname=" + System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(patient.Middlename.ToLower()) + ",snils=" + patient.Snils + ",enp=" + patient.Enp + " WHERE id=" + id + ";", connection);
                    command.ExecuteNonQuery();
                    connection.Close();
                }
                response = "{lasname:" + data.Lastname + ",/nfirstname:" + data.Firstname + ",/ nmiddlename:" + data.Middlename + ",/nsnils:" + data.Snils.ToString() + ",/nenp:" + data.Enp.ToString() + "}";

                return response;
            }
        }
    }

}