﻿using System;
using System.Collections.Generic;
using System.Text;

namespace httpserver
{
    public class Patient
    {
        public int Snils { get; set; }
        public string Lastname { get; set; }
        public string Firstname { get; set; }
        public string Middlename { get; set; }
        public int Enp { get; set; }
    }
}
